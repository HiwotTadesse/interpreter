﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Convert
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string response = "";
       
        public MainWindow()
        {
            InitializeComponent();
            
        }
        public int inputValue
        {
            get
            {
                return int.Parse( v.Text);
            }
            set
            {
                v.Text = value.ToString();
            }
        }

        private void result_Click(object sender, RoutedEventArgs e)
        {
            ConversionContext cc = new ConversionContext(inputValue);
            English eg = new English();
            response = eg.Inwords(cc);
            result.Text = response;
        }
    }
}
