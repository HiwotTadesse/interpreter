﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Convert
{
    class English : IExpression
    {
        public string Inwords(ConversionContext Inenglish)
        {
            return Inenglish.getResponse = NumberToWords(Inenglish.getInput);
        }

        public string NumberToWords(int number)
        {
            if (number == 0)
                return "Zero";
            if (number < 0)
                return "minus " + NumberToWords(Math.Abs(number));

            string words = "";
            if ((number / 1000000) > 0)
            {
                words += NumberToWords(number / 1000000) + " million ";
                number %= 1000000;
            }
            if ((number / 1000) > 0)
            {
                words += NumberToWords(number / 1000) + " thousand ";
                number %= 1000;
            }
            if ((number / 100) > 0)
            {
                words += NumberToWords(number / 100) + " hundred ";
                number %= 100;
            }
            if (number > 0)
            {
                if (words != "")
                    words += "and ";

                var digitsEn = new[] {"Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine","Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
                var tensEn = new[] { "Ten", "Tenty", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninty" };
                if (number < 20)
                    words += digitsEn[number];
                else
                {
                    words += tensEn[number / 10];
                    if ((number % 10) > 0)
                        words += "-" + digitsEn[number % 10];
                }
            }
            return words;
        }
    }
}
